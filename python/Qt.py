#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
<description>

:REQUIRES:

:TODO:

:FILE: Qt.py
:AUTHOR: blop
:CONTACT: zeliefr3d@gmail.com
:SINCE: 17/02/2022
:VERSION: 0.1
"""

# ==============================================================================
# METADATA
# ==============================================================================
__author__ = 'blop'
__contact__ = 'eliefr3d@gmail.com'
__date__ = '17/02/2022'
__version__ = '0.1'


# ==============================================================================
# IMPORT
# ==============================================================================

try:
    import PyQt5
    from PyQt5 import QtGui
    from PyQt5 import QtWidgets
    from PyQt5.QtCore import QFile
    from PyQt5.uic import loadUi
except ImportError as err:
    import PySide2
    from PySide2.QtUiTools import QUiLoader
    from PySide2 import QtWidgets
    from PySide2.QtCore import QFile


# ==============================================================================
# GLOBAL VARIABLES
# ==============================================================================
QT = [qt for qt in globals().keys() if qt in ['PyQt5', 'PySide2']][0]


# ==============================================================================
# METHODS
# ==============================================================================
def load_ui(q_file):
    if QT == 'PySide2':
        loader = QUiLoader()
        q_file.open(QFile.ReadOnly)
        
        return loader.load(q_file)
    else:
        q_file.open(QFile.ReadOnly)
        return loadUi(q_file)

# ==============================================================================
# MAIN METHOD AND TESTING AREA
# ==============================================================================
def main():
    """Description of main()"""
    return


if __name__ == '__main__':
    main()
