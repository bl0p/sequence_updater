#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
<description>

:REQUIRES:

:TODO:

:FILE: shotgun_connect.py
:AUTHOR: blop
:CONTACT: eliefr3d@gmail.com
:SINCE: 16/02/2022
:VERSION: 0.1
"""

# ==============================================================================
# METADATA
# ==============================================================================
__author__ = 'blop'
__contact__ = 'eliefr3d@gmail.com'
__date__ = '16/02/2022'
__version__ = '0.1'


# ==============================================================================
# IMPORT
# ==============================================================================


# ==============================================================================
# GLOBAL VARIABLES
# ==============================================================================


# ==============================================================================
# METHODS
# ==============================================================================
def get_script_connection(script_name):
    return sg()


class sg:
    def find(self, entity_type, filters=None, fields=None):
        if entity_type == 'Episode':
            return [{'type': 'Episode',
                     'id': 1245,
                     'code': 'ep001',
                     'sequences': [{'type': 'Sequence',
                                    'name': 'sq001',
                                    'id': 1},
                                   {'type': 'Sequence',
                                    'name': 'sq002',
                                    'id': 2}
                                   ]},
                    {'type': 'Episode',
                     'id': 1246,
                     'code': 'ep002',
                     'sequences': [{'type': 'Sequence',
                                    'name': 'sq001',
                                    'id': 3},
                                   {'type': 'Sequence',
                                    'name': 'sq002',
                                    'id': 4},
                                   {'type': 'Sequence',
                                    'name': 'sq003',
                                    'id': 5},
                                   {'type': 'Sequence',
                                    'name': 'sq004',
                                    'id': 6}
                                   ]},
                    {'type': 'Episode',
                     'id': 1247,
                     'code': 'ep003',
                     'sequences': [{'type': 'Sequence',
                                    'name': 'sq001',
                                    'id': 7},
                                   {'type': 'Sequence',
                                    'name': 'sq002',
                                    'id': 8}
                                   ]}]
        return []
    
    def create(self, *args, **kwargs):
        return
    
    def delete(self, *args, **kwargs):
        return


# ==============================================================================
# MAIN METHOD AND TESTING AREA
# ==============================================================================
def main():
    """Description of main()"""
    return


if __name__ == '__main__':
    main()
