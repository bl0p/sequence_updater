#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
<description>

:REQUIRES:

:TODO:

:FILE: sequence_updater.py
:AUTHOR: blop
:CONTACT: eliefr3d@gmail.com
:SINCE: 16/02/2022
:VERSION: 0.1
"""

# ==============================================================================
# METADATA
# ==============================================================================
__author__ = 'blop'
__contact__ = 'eliefr3d@gmail.com'
__date__ = '16/02/2022'
__version__ = '0.1'


# ==============================================================================
# IMPORT
# ==============================================================================
import os

import Qt
import shotgun_connect

from config import OMIT_STATUS
from config import SCRIPT_NAME
from config import SG_PROJECT

# ==============================================================================
# GLOBAL VARIABLES
# ==============================================================================
sg = shotgun_connect.get_script_connection(SCRIPT_NAME)


# ==============================================================================
# METHODS
# ==============================================================================
class BklUpdaterMainWindow(Qt.QtWidgets.QWidget):
    """
    Simple UI to synchronize sequence BreakDown List (BKL) with its shot's BKL
    """
    
    def __init__(self):
        super(BklUpdaterMainWindow, self).__init__(parent=None)
        
        # Loading file.ui
        ui_q_file = get_ui_QFile()
        self.main_window = Qt.load_ui(ui_q_file)
        
        # Show ui
        self.main_window.show()

        # Set UI
        self.ep_dict = {}
        self.get_sg_ep()  # set self.ep_dict
        episode_list = sorted(self.ep_dict.keys())

        self.main_window.ep_comboBox.addItems(episode_list)
        self.main_window.ep_comboBox.currentIndexChanged.connect(self.update_sequence_list)
        self.current_episode = str()
        self.update_sequence_list()
        self.main_window.ok_button.clicked.connect(self.ok_action)
    
    def get_sg_ep(self):
        """
        Stores episodes data from SG in self.ep_dict
        
        {'ep001': {'ep': {'type': 'Episode', 'id': 1245}, 'sequences': [{'type': 'Sequence', 'name': 'sq001', 'id': 1}]}}
        """
        # Get sequence data from DB
        ep_data = sg.find('Episode',
                          [['project', 'is', SG_PROJECT]],
                          ['code', 'sequences'])

        self.ep_dict = {ep['code']: {'ep': {'type': 'Episode', 'id': ep['id']}, 'sequences': ep['sequences']}
                        for ep in ep_data if ep['sequences']}
    
    def get_sequence_list(self):
        """
        Get a sequences list of choice for the episode chosen by customer
        :return: available sequences
        :rtype: list
        """
        # get selected episode
        self.current_episode = str(self.main_window.ep_comboBox.currentText())
        if not self.current_episode:
            return []
        # add 'ALL' as option to sequences available for this episode
        sq_list = ['ALL'] + sorted([x['name'] for x in self.ep_dict[self.current_episode]['sequences']])

        return sq_list
    
    def update_sequence_list(self):
        """
        Refresh the sequence combo box of the interface
        """
        self.update_message('')
        self.main_window.sq_comboBox.clear()
        squence_list = self.get_sequence_list()
        self.main_window.sq_comboBox.addItems(squence_list)
    
    def ok_action(self):
        """
        Launch Sequences update process
        """
        self.update_message('in progress...')
        self.set_sequences_bkl()
        self.update_message('Done')
    
    def set_sequences_bkl(self):
        """
        Edit sequence BKL by its shots BKL for those to treat
        """
        # Start by gets from the ui the sequence as SG entities
        current_sequence = str(self.main_window.sq_comboBox.currentText())
        if current_sequence == 'ALL':  # in the case all sequences of an episodes
            sg_sequence_list = self.ep_dict[self.current_episode]['sequences']
        else:
            sg_sequence_list = [x for x in self.ep_dict[self.current_episode]['sequences']
                                if x['name'] == current_sequence]

        # now we will compare the assets connected to sg_sequence with those connected to its shots
        # and delete obsolete AssetSequenceConnection or create theme if they are missing
        for sg_sequence in sg_sequence_list:
            sequence_bkl = {x['asset']['id']: x['id'] for x in
                            sg.find('AssetSequenceConnection', [['sequence', 'is', sg_sequence]], ['asset'])}
            shots_bkl = list(set([x['asset']['id'] for x in sg.find('AssetShotConnection',
                                                                    [['shot.Shot.sg_sequence', 'is', sg_sequence],
                                                                     ['shot.Shot.sg_status_list', 'is_not', OMIT_STATUS]],
                                                                    ['asset'])]))
            # Create missing asset/sequence connections
            for asset_id in shots_bkl:
                if asset_id not in sequence_bkl.keys():
                    data = {'sequence': sg_sequence, 'asset': {'type': 'Asset', 'id': asset_id}}
                    sg.create('AssetSequenceConnection', data)
            # Delete obsolete asset/sequence connections
            for asset_id in sequence_bkl.keys():
                if asset_id not in shots_bkl:
                    sg.delete('AssetSequenceConnection', sequence_bkl[asset_id])
    
    def update_message(self, message):
        """
        Edit the message to show
        
        :param str message: message to show
        """
        self.main_window.status_label.setText(message)


def get_ui_QFile():
    """
    Returns file.ui as an Qt object
    :return: the ui file
    :rtype: <>
    """
    this_path = os.path.dirname(__file__)
    ui_file = os.path.basename(__file__)
    ui_file = ui_file.replace(os.path.splitext(ui_file)[1], '.ui')
    ui_path = os.path.join(this_path, 'UI', ui_file)
    print(ui_path)
    if not os.path.exists(ui_path):
        raise IOError('path fail: {ui_path}'.format(ui_path=ui_path))
    return Qt.QFile(ui_path)


# ==============================================================================
# MAIN METHOD AND TESTING AREA
# ==============================================================================
def main():
    app = Qt.QtWidgets.QApplication(os.sys.argv)
    ex = BklUpdaterMainWindow()
    os.sys.exit(app.exec_())


if __name__ == '__main__':
    main()
