# simple project

This is a project that fake an update of a ShotGrid project.

This is not communicate with any SG project, it just simulate a connection.

#### Principes
The UI get from the ShotGrid a list of episodes and their sequences from the specified project (set in config.py).

The Customer chose an episode then the sequences that he want to synchronise.
Then, by clicking on "OK" button, the script will update the sequence's assets link according to its shots's assets link.

#### Test
##### From python ide
```
import sequence_updater

sequence_updater.main()
```